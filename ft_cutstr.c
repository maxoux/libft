/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cutstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaize <mlaize@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 16:20:26 by mlaize            #+#    #+#             */
/*   Updated: 2015/11/28 18:35:27 by mlaize           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_cutstr(char *str, size_t size)
{
	if (!str || size == 0)
		return (size);
	ft_strncpy(str, str + size, ft_strlen(str) - size);
	str[ft_strlen(str) - size] = 0;
	return (size);
}
