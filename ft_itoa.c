/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaize <mlaize@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 12:37:27 by mlaize            #+#    #+#             */
/*   Updated: 2015/03/17 10:40:28 by mlaize           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_itoa_setup(long *n)
{
	long	len;
	int		negative;
	char	*str;

	len = 1;
	negative = 0;
	if (*n < 0)
	{
		*n = -(*n);
		negative = 1;
	}
	while (ft_pow(10, len) < *n)
		len++;
	str = (char *)ft_memalloc(len + 2);
	if (negative)
		str[0] = '-';
	return (str);
}

char		*ft_itoa(int n)
{
	long		number;
	long		pow;
	char		*str;

	number = n;
	pow = 1;
	str = ft_itoa_setup(&number);
	while (pow * 10 < number)
		pow *= 10;
	while (pow > 0)
	{
		ft_memappend(str, (char)(((long)number / (long)pow % 10) + '0'));
		pow /= 10;
	}
	return (str);
}
