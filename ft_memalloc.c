/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaize <mlaize@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 10:02:23 by mlaize            #+#    #+#             */
/*   Updated: 2015/03/17 10:40:04 by mlaize           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	char		*memory;
	size_t		i;

	memory = malloc(size);
	if (memory == NULL)
		return (NULL);
	i = 0;
	while (i < size)
	{
		memory[i] = 0;
		i++;
	}
	return ((void *)memory);
}
